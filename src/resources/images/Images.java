/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources.images;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author eryalus
 */
public class Images {

    public URL getLogo() {
        return this.getClass().getResource("logo.png");
    }

    public Image getLogoAsImage() {
        return new ImageIcon(getLogo()).getImage();
    }

    public ImageIcon getLogoAsIcon() {
        return new ImageIcon(getLogo());
    }

    public URL getInfoLogo() {
        return this.getClass().getResource("info_large.png");
    }

    public Image getInfoLogoAsImage() {
        return new ImageIcon(getInfoLogo()).getImage();
    }

    public ImageIcon getInfoLogoAsIcon() {
        return new ImageIcon(getInfoLogo());
    }

    public URL getErrorLogo() {
        return this.getClass().getResource("error_large.png");
    }

    public Image getErrorLogoAsImage() {
        return new ImageIcon(getErrorLogo()).getImage();
    }

    public ImageIcon getErrorLogoAsIcon() {
        return new ImageIcon(getErrorLogo());
    }

    public URL getQuestionLogo() {
        return this.getClass().getResource("question_large.png");
    }

    public Image getQuestionLogoAsImage() {
        return new ImageIcon(getQuestionLogo()).getImage();
    }

    public ImageIcon getQuestionLogoAsIcon() {
        return new ImageIcon(getQuestionLogo());
    }
}
