/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import resources.images.Images;
import rsrclient.UAC.UAC_Manager;
import rsrclient.cleaner.Cleaner;
import utils.frames.AboutPane;
import utils.frames.ChangelogPane;

/**
 *
 * @author eryalus
 */
public class PanelEnvio extends javax.swing.JFrame {

    public static final String DEFAULT_IP = "127.0.0.1", DEFAULT_PORT = "10524";
    private String ip = DEFAULT_IP, aula = "";
    private Integer port = 10000, num_pc = null;
    private final String FICHERO_CONF = "rsr.cnf";
    private static final String INICIO_PUERTO = "port", INICIO_IP = "ip", INICIO_AULA = "pc", INICIO_NUM = "num";

    private void setInfoRed() {
        etinfo.setText(ip + ":" + port);
    }

    private void setInfoPC() {
        tanombre.setText(aula);
        if (num_pc != null) {
            tanumero.setText("" + num_pc);
        }
    }

    @Override
    public void setEnabled(boolean flag) {
        jButton2.setEnabled(flag);
        jMenuItem12.setEnabled(flag);
    }

    private boolean writeInfoToFile(String IP, String puerto, String aula, Integer numero) {
        File file = new File(FICHERO_CONF);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
            }
        }
        ArrayList<String> lineas = new ArrayList<>();
        lineas.add(INICIO_PUERTO + "=" + puerto);
        lineas.add(INICIO_IP + "=" + IP);
        lineas.add(INICIO_AULA + "=" + aula);
        lineas.add(INICIO_NUM + "=" + numero);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(FICHERO_CONF));
            for (String s : lineas) {
                bw.write(s + "\n");
            }
            bw.close();
            setInfoRed();
            return true;
        } catch (IOException ex) {
            error("No se ha podido escribir el fichero de configuración.\n0x003");
            return false;
        }

    }

    private void readInfoFromFile() {
        File file = new File(FICHERO_CONF);
        boolean write = false;
        if (file.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(FICHERO_CONF));
                String line;
                boolean port_ok = false, ip_ok = false, aula_ok = false, num_ok = false;
                while ((line = br.readLine()) != null) {
                    if (line.trim().startsWith(INICIO_PUERTO)) {
                        String[] parts = line.split("=");
                        if (parts.length == 2) {
                            try {
                                port = Integer.parseInt(parts[1]);
                                port_ok = true;
                            } catch (NumberFormatException ex) {

                            }
                        }
                    } else if (line.trim().startsWith(INICIO_IP)) {
                        String[] parts = line.split("=");
                        if (parts.length == 2) {
                            ip = parts[1];
                            ip_ok = true;
                        }
                    } else if (line.trim().startsWith(INICIO_AULA)) {
                        String[] parts = line.split("=");
                        if (parts.length == 2) {
                            aula = parts[1];
                            aula_ok = true;
                        }
                    } else if (line.trim().startsWith(INICIO_NUM)) {
                        String[] parts = line.split("=");
                        if (parts.length == 2) {
                            if (parts[1].equals("")) {
                                num_pc = null;
                                num_ok = true;
                            } else {
                                try {
                                    num_pc = Integer.parseInt(parts[1]);
                                    num_ok = true;
                                } catch (NumberFormatException ex) {

                                }
                            }
                        }
                    }
                }
                br.close();
                if (!(port_ok && ip_ok)) {
                    error("No se ha podido leer el fichero de configuración.\n0x000");
                    System.exit(0);
                }
            } catch (IOException ex) {
                error("No se ha podido leer el fichero de configuración.\n0x001");
                System.exit(0);
            }
        } else {
            try {
                file.createNewFile();
                write = true;
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "No se ha podido leer el fichero de configuración.\n0x002", "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
        }
        if (write) {
            if (writeInfoToFile(DEFAULT_IP, DEFAULT_PORT, "", null)) {
                ip = DEFAULT_IP;
                port = Integer.parseInt(DEFAULT_PORT);
                setInfoRed();
            }
        }
    }

    public PanelEnvio() {
        initComponents();
        readInfoFromFile();
        setInfoRed();
        setInfoPC();
        try {
            String name = InetAddress.getLocalHost().getHostName();
            String[] partes = name.split("-");
            if (partes.length == 2) {
                String aula = partes[0].trim();
                Integer num = Integer.parseInt(partes[1].trim());
                tanombre.setText(aula);
                tanumero.setText("" + num);
            }
        } catch (UnknownHostException | NumberFormatException ex) {
        }
    }

    public void autoCheck() {
        RSRClient.checkVersion(ip, port, this, 1);
    }

    public enum Status {
        REPOSO, ENVIANDO, ENVIADO, LEYENDO, CONECTANDO, ERROR, INIT_CIPHER, AUTH
    };
    private final String REP = "Reposo", ENVIAN = "Enviando...", ENVIA = "Enviado",
            LEY = "Leyendo datos...", CON = "Conectado...", ERR = "Error",
            INIT_C = "I. cifrado...", AUT = "Autenticando...";

    private void error(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE, new Images().getErrorLogoAsIcon());
    }

    private void enviar() {
        if (!jButton2.isEnabled()) {
            return;
        }
        String aula = tanombre.getText(), numero = tanumero.getText();

        if (aula.equals("")) {
            error("Debe introducir el nombre del aula.");
            return;
        }
        if (numero.equals("")) {
            error("Debe introducir el número del equipo.");
            return;
        }
        try {
            Integer.parseInt(numero);
        } catch (NumberFormatException ex) {
            error("Debe introducir el número válido para el equipo.");
            return;
        }
        writeInfoToFile(ip, "" + port, aula, Integer.parseInt(numero));
        RSRClient hilo_envio = new RSRClient(ip, port, aula, numero, this);
        hilo_envio.start();
    }

    public synchronized void setStatus(Status status) {
        switch (status) {
            case REPOSO:
                jLabel4.setText(REP);
                break;
            case ENVIADO:
                jLabel4.setText(ENVIA);
                break;
            case ENVIANDO:
                jLabel4.setText(ENVIAN);
                break;
            case CONECTANDO:
                jLabel4.setText(CON);
                break;
            case LEYENDO:
                jLabel4.setText(LEY);
                break;
            case ERROR:
                jLabel4.setText(ERR);
                break;
            case AUTH:
                jLabel4.setText(AUT);
                break;
            case INIT_CIPHER:
                jLabel4.setText(INIT_C);
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        etinfo = new javax.swing.JLabel();
        tanumero = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        tanombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem12 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem7 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setRequestFocusEnabled(false);

        jLabel4.setText("Reposo");

        jLabel3.setText("Estado:");

        etinfo.setText("localhost:0");

        tanumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tanumeroActionPerformed(evt);
            }
        });
        tanumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tanumeroKeyTyped(evt);
            }
        });

        jLabel1.setText("Número de equipo en el aula:");

        tanombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tanombreKeyTyped(evt);
            }
        });

        jLabel2.setText("Nombre de aula (e.g. AIE):");

        jButton2.setText("Enviar datos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(etinfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tanumero, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                        .addGap(18, 18, 18))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tanombre)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tanombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tanumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etinfo)
                .addContainerGap())
        );

        jMenu1.setText("Archivo");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/ip.png"))); // NOI18N
        jMenuItem1.setText("Cambiar IP");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/port.png"))); // NOI18N
        jMenuItem2.setText("Cambiar puerto");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator2);

        jMenuItem12.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/plane.png"))); // NOI18N
        jMenuItem12.setText("Enviar");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem12);
        jMenu1.add(jSeparator1);

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/info.png"))); // NOI18N
        jMenuItem7.setText("Actual");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);
        jMenu1.add(jSeparator3);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/exit.png"))); // NOI18N
        jMenuItem3.setText("Salir");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Herramientas");

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/broom.png"))); // NOI18N
        jMenu5.setText("Limpiador");

        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/config.png"))); // NOI18N
        jMenuItem9.setText("Configurar");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem9);

        jMenuItem10.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/run.png"))); // NOI18N
        jMenuItem10.setText("Ejecutar");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem10);

        jMenu4.add(jMenu5);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/update.png"))); // NOI18N
        jMenu2.setText("Actualizaciones");

        jMenuItem11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/info2.png"))); // NOI18N
        jMenuItem11.setText("Última versión");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem11);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/check_update.png"))); // NOI18N
        jMenuItem5.setText("Comprobar");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/force_update.png"))); // NOI18N
        jMenuItem6.setText("Forzar actualización");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenu4.add(jMenu2);

        jMenuBar1.add(jMenu4);

        jMenu3.setText("Ayuda");

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/changelog.png"))); // NOI18N
        jMenuItem4.setText("Changelog");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/about.png"))); // NOI18N
        jMenuItem8.setText("About");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        String jop = (String) JOptionPane.showInputDialog(this, "Introduzca la nueva IP:", "", JOptionPane.PLAIN_MESSAGE, new Images().getQuestionLogoAsIcon(), null, null);
        if (jop != null) {
            try {
                InetAddress nip = InetAddress.getByName(jop);
                ip = jop;
                writeInfoToFile(ip, "" + port, aula, num_pc);
            } catch (UnknownHostException ex) {
                error("IP inválida");
            }
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        String jop = (String) JOptionPane.showInputDialog(this, "Introduzca el nuevo puerto:", "", JOptionPane.PLAIN_MESSAGE, new Images().getQuestionLogoAsIcon(), null, null);
        if (jop != null) {
            try {
                Integer puerto_usu = Integer.parseInt(jop);
                if (puerto_usu <= 65535 && puerto_usu >= 0) {
                    port = puerto_usu;
                    writeInfoToFile(ip, "" + port, aula, num_pc);
                } else {
                    error("No ha introducido un número válido");
                }
            } catch (NumberFormatException ex) {
                error("No ha introducido un número");
            }
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        enviar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        RSRClient.checkVersion(ip, port, this, 2);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        show_info();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void show_info() {
        String txt = "RSR Client v" + RSRClient.getVers() + "\n";
        txt += "Conexión " + ip + ":" + port;
        JOptionPane.showMessageDialog(this, txt, "Info", JOptionPane.INFORMATION_MESSAGE, new Images().getInfoLogoAsIcon());
    }
    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        int op = JOptionPane.showConfirmDialog(this, "¿Desea forzar una actualización de la aplicación?", "Actualización", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new Images().getQuestionLogoAsIcon());
        if (op == JOptionPane.YES_OPTION) {
            try {
                RSRClient.updateVers(ip, port, this, 2);
            } catch (IOException ex) {
                ex.printStackTrace();
                error("No se ha podido conectar con el servidor");
            }
        }
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void tanumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tanumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tanumeroActionPerformed

    private void tanumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tanumeroKeyTyped
        if (evt.getKeyChar() == '\n') {
            enviar();
        }
    }//GEN-LAST:event_tanumeroKeyTyped

    private void tanombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tanombreKeyTyped
        if (evt.getKeyChar() == '\n') {
            enviar();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_tanombreKeyTyped

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        changelog();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        about();
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        configCleaner();        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        try {
            if (new UAC_Manager().checkUAC()) {
                runCleaner();
            } else {
                int op = JOptionPane.showConfirmDialog(this, "Ejecutar como administrador", "", JOptionPane.YES_NO_OPTION);
                if (op == JOptionPane.YES_OPTION) {
                    new UAC_Manager().execute_as_admin();
                }
            }
            //         // TODO add your handling code here:
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(PanelEnvio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        try {
            RSRClient.getLatestVers(ip, port, this);        // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(PanelEnvio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        enviar();
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void runCleaner() {
        Cleaner cleaner = new Cleaner(this);
        cleaner.run();
    }

    private void configCleaner() {
        Cleaner cleaner = new Cleaner(this);
        cleaner.config();
    }

    private void changelog() {
        ChangelogPane pane = new ChangelogPane(this);
        pane.setLocationRelativeTo(this);
        pane.setVisible(true);
    }

    private void about() {
        AboutPane pane = new AboutPane(this);
        pane.setLocationRelativeTo(this);
        pane.setVisible(true);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel etinfo;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JTextField tanombre;
    private javax.swing.JTextField tanumero;
    // End of variables declaration//GEN-END:variables
}
