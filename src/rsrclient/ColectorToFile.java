/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class ColectorToFile extends Colector {

    BufferedWriter bw;

    public ColectorToFile(String comando, Semaphore semaforo, String destFile) throws IOException {
        super(comando, semaforo);
        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(destFile), StandardCharsets.UTF_8));

    }

    @Override
    public void run() {
        try {
            Process proceso = Runtime.getRuntime().exec(COMANDO);
            BufferedReader reader = new BufferedReader(new InputStreamReader(proceso.getInputStream()));

            String linea;
            while ((linea = reader.readLine()) != null) {
                bw.write(linea + "\n");
            }
            bw.flush();
        } catch (IOException ex) {
            error = true;
        } finally {
            try {
                bw.close();
            } catch (IOException ex) {
                Logger.getLogger(ColectorToFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            SEM.release();
        }
    }

}
