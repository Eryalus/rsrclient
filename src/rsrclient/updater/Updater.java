/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient.updater;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import resources.images.Images;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class Updater {

    private final SocketReader READER;
    private final SocketWriter WRITER;
    private final JFrame PARENT;
    private final String IP;
    private final int PORT;
    private final Socket SOC;
    private static final int UPDATER_ACCESS_CODE = 4;
    private static final String CHECK_MY_VERSION = "check_version";
    private static final String GET_LATEST = "get_latest";
    private static final String UPDATE_VERSION = "update_version";

    public Updater(JFrame parent, String ip, int port) throws IOException {
        SOC = new Socket(ip, port);
        SocketReader reader = new SocketReader(SOC);
        SocketWriter writer = new SocketWriter(SOC);
        READER = reader;
        WRITER = writer;
        PARENT = parent;
        IP = ip;
        PORT = port;
        setup_connection();
    }

    public void close_connection() throws IOException {
        SOC.close();
    }

    private void setup_connection() throws IOException {
        WRITER.writeInt32(UPDATER_ACCESS_CODE);
    }

    public void check_latest_version() throws IOException {
        WRITER.writeString(GET_LATEST);
        WRITER.flush();
        long code = READER.readLong();
        if (code == 0) {
            String txt = READER.readString();
            JOptionPane.showMessageDialog(PARENT, txt, "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            String txt = READER.readString();
            error("Error: " + txt + "\nCódigo de error:" + code);
        }
        SOC.close();
    }

    public void update_version() throws IOException {
        WRITER.writeString(UPDATE_VERSION);
        WRITER.flush();
        long code = READER.readLong();
        if (code == 0) {
            File base = new File(System.getProperty("user.dir"));
            File zip_file = new File(System.getProperty("user.dir") + "/temp.zip");
            File bat_file = new File(System.getProperty("user.dir") + "/temp.bat");
            READER.readFile(bat_file);
            READER.readFile(zip_file);
            unzip(base, zip_file);
            zip_file.delete();
            try {
                Runtime.getRuntime().exec("temp.bat " + bat_file.getAbsolutePath());
                SOC.close();
                System.exit(0);
            } catch (IOException ex) {
                error("No se ha podido ejecutar el actualizador.");
                SOC.close();
            }
        } else {
            String txt = READER.readString();
            error("Error: " + txt + "\nCódigo de error:" + code);
        }
    }

    private static void unzip(File destDir, File origin) throws FileNotFoundException, IOException {
        String fileZip = origin.getAbsolutePath();
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                newFile.mkdirs();
            } else {
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    public void check_my_version(int verbose) throws IOException {
        WRITER.writeString(CHECK_MY_VERSION);
        WRITER.flush();
        long code = READER.readLong();
        if (code == 0) {
            WRITER.writeString(rsrclient.RSRClient.getVers());
            WRITER.flush();
            Integer update_code = READER.readInt32();
            String msg = READER.readString();
            if (update_code == 0) {
                if (verbose > 1) {
                    JOptionPane.showMessageDialog(PARENT, msg, "", JOptionPane.INFORMATION_MESSAGE, new Images().getInfoLogoAsIcon());
                }
            } else if (verbose > 0) {
                int option = JOptionPane.showConfirmDialog(PARENT, msg + "\nDesea actualizar?", "", JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.YES_OPTION) {
                    try {
                        new Updater(PARENT, IP, PORT).update_version();
                    } catch (IOException ex) {
                        if (verbose > 0) {
                            JOptionPane.showMessageDialog(PARENT, "No se ha podido conectar con el servidor", "ERROR", JOptionPane.ERROR_MESSAGE, new Images().getErrorLogoAsIcon());
                        }
                    }
                }
            }

        } else {
            String txt = READER.readString();
            error("Error: " + txt + "\nCódigo de error:" + code);
        }

        SOC.close();
    }

    private void error(String s) {
        JOptionPane.showMessageDialog(PARENT, s, "ERROR", JOptionPane.ERROR_MESSAGE);
    }
}
