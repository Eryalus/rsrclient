/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient;

import utils.frames.PasswordPane;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import resources.images.Images;
import rsrclient.updater.Updater;
import utils.cipher.DH;
import utils.cipher.Encryptor;
import utils.data.Disk;
import utils.data.Programa;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;
import utils.threads.SingleThread;

/**
 *
 * @author eryalus
 */
public class RSRClient extends SingleThread {

    public static final String VERSION = "3.0";
    private static final String temp = System.getProperty("java.io.tmpdir");
    private final String IP, AULA, NUMERO;
    private final int PORT;
    private final PanelEnvio PARENT;

    public RSRClient(String IP, Integer PORT, String aula, String numero, PanelEnvio parent) {
        super.NOMBRE_HILO = "Hilo de conexión";
        this.IP = IP;
        this.PORT = PORT;
        this.PARENT = parent;
        this.AULA = aula;
        this.NUMERO = numero;
    }

    private void all(Socket soc, SocketReader reader, SocketWriter writer) throws IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        writer.writeInt32(3);
        writer.writeString(VERSION);
        Long resp = reader.readLong();
        if (resp < 0) {
            String error_txt = reader.readString();
            JOptionPane.showMessageDialog(null, error_txt + "\n\nError code: " + resp, "ERROR", JOptionPane.ERROR_MESSAGE, new Images().getErrorLogoAsIcon());
            return;
        }
        PARENT.setStatus(PanelEnvio.Status.INIT_CIPHER);
        DH diffie = new DH(writer, reader);
        BigInteger shared = diffie.handShake_Alice();
        PARENT.setStatus(PanelEnvio.Status.AUTH);
        Encryptor encryptor = new Encryptor(shared.toString());
        while (true) {
            String pass = PasswordPane.getPassword(null);
            if (pass == null) {
                writer.writeLong(0);
                writer.flush();
                PARENT.setStatus(PanelEnvio.Status.REPOSO);
                PARENT.setEnabled(true);
                return;
            }
            writer.writeLong(1);
            writer.flush();
            Encryptor encryptor_pass = new Encryptor(pass);
            String reto = encryptor.decrypt(reader.readString());
            String reto_e = encryptor_pass.encrypt(reto);
            writer.writeString(encryptor.encrypt(reto_e));
            writer.flush();
            if (reader.readLong() == 1) {
                break;
            }
        }
        PARENT.setEnabled(false);
        
        PARENT.setStatus(PanelEnvio.Status.LEYENDO);
        writer.writeString(encryptor.encrypt(AULA.toUpperCase()));
        writer.writeString(encryptor.encrypt(addCeros(NUMERO, 2)));
        Long num = reader.readLong();
        if (num != 0) {
            int op = JOptionPane.showConfirmDialog(PARENT, "Ya hay datos de ese aula.\nDesea sobreescribirlo?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new Images().getQuestionLogoAsIcon());
            if (!(op == JOptionPane.YES_OPTION)) {
                PARENT.setStatus(PanelEnvio.Status.ERROR);
                PARENT.setEnabled(true);
                writer.writeLong(1);
                writer.close();
                reader.close();
                soc.close();
                return;
            }
        }
        writer.writeLong(0);
        PARENT.setStatus(PanelEnvio.Status.LEYENDO);

        Colector[] colectores = new Colector[8];
        Semaphore[] semaforos = new Semaphore[8];
        for (int i = 0; i < semaforos.length; i++) {
            semaforos[i] = new Semaphore(0);
        }
        colectores[0] = new ColectorToFile("wmic cpu get Name,NumberOfCores /format:list", semaforos[0], temp + "/cpu.txt");
        colectores[0].start();
        colectores[1] = new ColectorToFile("wmic baseboard get product /format:list", semaforos[1], temp + "/mother.txt");
        colectores[1].start();
        colectores[2] = new ColectorToFile("wmic path win32_VideoController get name /format:list", semaforos[2], temp + "/videocard.txt");
        colectores[2].start();
        colectores[3] = new ColectorToFile("wmic computersystem get TotalPhysicalMemory /format:list", semaforos[3], temp + "/ram.txt");
        colectores[3].start();
        colectores[4] = new ColectorToFile("wmic diskdrive get model,size /format:list", semaforos[4], temp + "/disk.txt");
        colectores[4].start();
        colectores[5] = new ColectorToFile("wmic product get name,version,description,vendor /format:list", semaforos[5], temp + "/software.txt");
        colectores[5].start();
        colectores[6] = new ColectorToFile("getmac /FO csv", semaforos[6], temp + "/mac.txt");
        colectores[6].start();
        colectores[7] = new ColectorToFile("wmic logicaldisk get freespace,name,size /format:csv", semaforos[7], temp + "/partitions.txt");
        colectores[7].start();
        InetAddress localIP = utils.networking.IPv4.getLansIP();
        for (int i = 0; i < semaforos.length; i++) {
            synchronized (semaforos[i]) {
                try {
                    semaforos[i].acquire();
                } catch (InterruptedException ex) {
                    error("Error obteniendo datos.\n0x004");
                    endError();
                    return;
                }
            }
        }
        boolean error = false;
        for (Colector col : colectores) {
            if (col.hasError()) {
                System.out.println(col.getComando());
            }
            error = error | col.hasError();
        }
        if (error) {
            error("Error recolectando datos.\n0x005");
            endError();
            return;
        }
        PARENT.setStatus(PanelEnvio.Status.ENVIANDO);
        String tmp;
        ArrayList<Programa> programs = new ArrayList<>();
        ArrayList<Disk> discos = new ArrayList<>();
        String nombre_procesador = null, mother = null, videocard = null, disk_model = null;
        Integer cores = null;
        Long ramsize = null, disk_size = null;
        BufferedReader br = new BufferedReader(new FileReader(temp + "/software.txt"));
        Programa p = new Programa();
        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split("=");
            if (partecitas.length >= 2) {
                switch (partecitas[0]) {
                    case "Name":
                        p.setName(partecitas[1]);
                        break;
                    case "Description":
                        p.setDescription(partecitas[1]);
                        break;
                    case "Vendor":
                        p.setVendor(partecitas[1]);
                        break;
                    case "Version":
                        p.setVersion(partecitas[1]);
                        break;
                    default:
                        break;
                }
                if (p.getName() != null && p.getVendor() != null && p.getVersion() != null && p.getVendor() != null) {
                    programs.add(p);
                    p = new Programa();
                }
            }
        }
        br.close();
        br = new BufferedReader(new FileReader(temp + "/cpu.txt"));

        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split("=");
            switch (partecitas[0]) {
                case "Name":
                    nombre_procesador = partecitas[1];
                    break;
                case "NumberOfCores":
                    try {
                        cores = Integer.parseInt(partecitas[1]);
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        }
        br.close();
        br = new BufferedReader(new FileReader(temp + "/mother.txt"));

        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split("=");
            if (partecitas[0].equals("Product")) {
                mother = partecitas[1];
                break;
            }
        }
        br.close();
        br = new BufferedReader(new FileReader(temp + "/videocard.txt"));

        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split("=");
            if (partecitas[0].equals("Name")) {
                videocard = partecitas[1];
            }
        }
        br.close();
        br = new BufferedReader(new FileReader(temp + "/ram.txt"));

        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split("=");
            if (partecitas[0].equals("TotalPhysicalMemory")) {
                try {
                    ramsize = Long.parseLong(partecitas[1]);
                } catch (NumberFormatException es) {

                }
            }
        }
        br.close();
        br = new BufferedReader(new FileReader(temp + "/disk.txt"));

        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split("=");
            try {
                if (partecitas[0].equals("Model")) {
                    disk_model = partecitas[1];
                } else if (partecitas[0].equals("Size")) {
                    try {
                        disk_size = Long.parseLong(partecitas[1]);
                    } catch (NumberFormatException ex) {
                        disk_model = null;
                    }
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                disk_model = null;
            }
            if (disk_model != null && disk_size != null) {
                discos.add(new Disk(disk_size, disk_model));
                disk_size = null;
                disk_model = null;
            }
        }
        br.close();
        br = new BufferedReader(new FileReader(temp + "/mac.txt"));
        String mac = "no";
        br.readLine();
        while ((tmp = br.readLine()) != null) {
            String[] partecitas = tmp.split(",");
            if (partecitas.length == 2) {
                mac = partecitas[0].replace("\"", "").replace("-", ":");
                break;
            }
        }
        br.close();
        ArrayList<String> logicaldrives = new ArrayList<>();
        br = new BufferedReader(new FileReader(temp + "/partitions.txt"));
        String tmp_line;
        while ((tmp_line = br.readLine()) != null) {

            if (tmp_line.trim().equals("")) {
                continue;
            }
            if (tmp_line.trim().startsWith("Node,Free")) {
                continue;
            }
            logicaldrives.add(tmp_line);
        }
        br.close();
        String IP = "No disponible";
        if (localIP != null) {
            IP = localIP.getHostAddress();
        }
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element root = doc.createElement("computer");
        root.setAttribute("class", AULA.toUpperCase());
        root.setAttribute("number", NUMERO);
        root.setAttribute("ip", IP);
        //processor
        Element processor = doc.createElement("processor");
        Element processor_name = doc.createElement("name");
        processor_name.appendChild(doc.createTextNode(nombre_procesador));
        processor.appendChild(processor_name);
        Element processor_number_cores = doc.createElement("cores");
        processor_number_cores.appendChild(doc.createTextNode("" + cores));
        processor.appendChild(processor_number_cores);
        root.appendChild(processor);
        //motherboard
        Element motherboard = doc.createElement("motherboard");
        motherboard.appendChild(doc.createTextNode(mother));
        root.appendChild(motherboard);
        //videocard
        Element videocard_e = doc.createElement("videocard");
        videocard_e.appendChild(doc.createTextNode(videocard));
        root.appendChild(videocard_e);
        //ram
        Element ram = doc.createElement("ramsize");
        ram.appendChild(doc.createTextNode("" + ramsize));
        root.appendChild(ram);
        //mac
        Element mac_e = doc.createElement("mac");
        mac_e.appendChild(doc.createTextNode(mac));
        root.appendChild(mac_e);
        //disks
        Element discks = doc.createElement("physicaldisks");
        for (Disk d : discos) {
            Element disk = doc.createElement("disk");
            Element name = doc.createElement("name");
            name.appendChild(doc.createTextNode(d.getName()));
            disk.appendChild(name);
            Element size = doc.createElement("capacity");
            size.appendChild(doc.createTextNode("" + d.getSize()));
            disk.appendChild(size);
            discks.appendChild(disk);
        }
        root.appendChild(discks);
        //partitions
        Element partitions = doc.createElement("partitions");
        for (String s : logicaldrives) {
            String[] pts = s.split(",");
            if (pts.length == 4) {
                String name, free, total;
                name = pts[2];
                try {
                    free = "" + Long.parseLong(pts[1]);
                    total = "" + Long.parseLong(pts[3]);
                    Element partition = doc.createElement("partition");
                    Element name_e = doc.createElement("name");
                    name_e.appendChild(doc.createTextNode(name));
                    partition.appendChild(name_e);
                    Element free_e = doc.createElement("freespace");
                    free_e.appendChild(doc.createTextNode(free));
                    partition.appendChild(free_e);
                    Element total_e = doc.createElement("size");
                    total_e.appendChild(doc.createTextNode(total));
                    partition.appendChild(total_e);
                    partitions.appendChild(partition);
                } catch (NumberFormatException ex) {

                }
            }
        }
        root.appendChild(partitions);
        //programs
        Element programs_e = doc.createElement("programs");
        for (Programa prog : programs) {
            Element program = doc.createElement("program");
            Element vendor = doc.createElement("vendor");
            Element version = doc.createElement("version");
            Element name = doc.createElement("name");
            Element desc = doc.createElement("description");
            vendor.appendChild(doc.createTextNode(prog.getVendor()));
            version.appendChild(doc.createTextNode(prog.getVersion()));
            name.appendChild(doc.createTextNode(prog.getName()));
            desc.appendChild(doc.createTextNode(prog.getDescription()));
            program.appendChild(vendor);
            program.appendChild(version);
            program.appendChild(name);
            program.appendChild(desc);
            programs_e.appendChild(program);
        }
        root.appendChild(programs_e);
        doc.appendChild(root);
        //write it
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(temp + "/file.xml"));
        transformer.transform(source, result);
        byte[] file_b = Files.readAllBytes(new File(temp + "/file.xml").toPath());
        byte[] file_e = encryptor.encrypt(file_b);
        writer.writeLong(file_e.length);
        writer.write(file_e);
        Long response = reader.readLong();
        new File(temp + "/cpu.txt").delete();
        new File(temp + "/mother.txt").delete();
        new File(temp + "/videocard.txt").delete();
        new File(temp + "/ram.txt").delete();
        new File(temp + "/disk.txt").delete();
        new File(temp + "/software.txt").delete();
        new File(temp + "/mac.txt").delete();
        new File(temp + "/partitions.txt").delete();
        new File(temp + "/file.xml").delete();
        if (response != 0) {
            error(reader.readString());
            endError();
            return;
        }
        PARENT.setStatus(PanelEnvio.Status.ENVIADO);
        PARENT.setEnabled(true);
    }

    private String addCeros(String txt, int tamaño) {
        int toadd = tamaño - txt.length();
        if (toadd > 0) {
            String temp = "";
            for (int i = 0; i < toadd; i++) {
                temp += "0";
            }
            temp += txt;
            return temp;
        } else {
            return txt;
        }
    }

    public static String getVers() {
        return VERSION;
    }

    public static void getLatestVers(String IP, int port, JFrame PARENT) throws IOException {
        Updater updater = new Updater(PARENT, IP, port);
        updater.check_latest_version();
    }

    public static void updateVers(String IP, int port, JFrame PARENT, int verbose) throws IOException {
        Updater updater = new Updater(PARENT, IP, port);
        updater.update_version();
    }

    public static void checkVersion(String IP, int port, PanelEnvio PARENT, int verbose) {
        try {
            Updater updater = new Updater(PARENT, IP, port);
            updater.check_my_version(verbose);
        } catch (IOException ex) {
            if (verbose > 1) {
                JOptionPane.showMessageDialog(PARENT, "No se ha podido conectar con el servidor", "ERROR", JOptionPane.ERROR_MESSAGE, new Images().getErrorLogoAsIcon());
            }
        }
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            PARENT.setEnabled(false);
            PARENT.setStatus(PanelEnvio.Status.CONECTANDO);
            Socket soc = new Socket(IP, PORT);
            SocketReader reader = new SocketReader(soc);
            SocketWriter writer = new SocketWriter(soc);

            all(soc, reader, writer);

            writer.close();
            reader.close();
            soc.close();

        } catch (IOException ex) {
            error(ex.getMessage() + "\nNo se ha podido conectar con el servidor.\n0x006");
            endError();
        } catch (ParserConfigurationException | TransformerException ex) {
            error(ex.getMessage() + "\nNo se ha podido crear el report.\n0x007");
            endError();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(RSRClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void endError() {
        PARENT.setStatus(PanelEnvio.Status.ERROR);
        PARENT.setEnabled(true);
    }

    private void error(String msg) {
        JOptionPane.showMessageDialog(PARENT, msg, "Error", JOptionPane.ERROR_MESSAGE, new Images().getErrorLogoAsIcon());
    }
}
