/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient.cleaner;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import rsrclient.PanelEnvio;

/**
 *
 * @author ad_ri
 */
public class Cleaner {

    private final PanelEnvio PARENT;
    private static final String PATH_CONFIG_FILE = "cleaner.cnf";
    private Long bytes_deleted = 0L, number_of_files = 0L, number_of_folders = 0L;

    public Cleaner() {
        PARENT = null;
    }

    public Cleaner(PanelEnvio parent) {
        PARENT = parent;
    }

    /**
     * Run the cleaner
     */
    public void run() {

        if (!new File(PATH_CONFIG_FILE).exists()) {
            try {
                createDefaultCleaner();
            } catch (ParserConfigurationException | TransformerException ex) {
                Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            ArrayList<Rule> rules = new ArrayList<>();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new File(PATH_CONFIG_FILE));
            //read data
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/rules/rule";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(
                    doc, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                String type = node.getAttributes().getNamedItem("type").getNodeValue();
                String path = "";
                ArrayList<String> excludeds = new ArrayList<>();
                for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                    Node node2 = node.getChildNodes().item(j);
                    if (node2.getNodeName().equals("path")) {
                        path = node2.getChildNodes().item(0).getNodeValue();
                    } else if (node2.getNodeName().equals("excluded")) {
                        excludeds.add(node2.getChildNodes().item(0).getNodeValue());
                    }
                }
                rules.add(new Rule(type, path, excludeds));
            }
            ArrayList<String> errors = new ArrayList<>();
            for (Rule rule : rules) {
                try {
                    runForRule(rule);
                } catch (PatternSyntaxException ex) {
                    errors.add("Error en " + rule.path + "\n" + ex.getMessage());
                }
            }
            for (String error : errors) {
                JOptionPane.showMessageDialog(PARENT, error, "ERROR", JOptionPane.ERROR_MESSAGE);
            }
            JOptionPane.showMessageDialog(PARENT, "El limpiador ha finalizado\n"
                    + "Se han borrado " + number_of_files + " ficheros.\n"
                    + "Se han borrado " + number_of_folders + " carpetas.\n"
                    + "Se han borrado " + getSize(bytes_deleted), "Fin", JOptionPane.INFORMATION_MESSAGE
            );
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getSize(Long size) {
        if (size < 1000) {
            return "" + size;
        } else if (size < 1000000) {
            return (size / 1000) + "kB";
        } else {
            return (size / 1000000) + "MB";
        }
    }

    private void runForRule(Rule rule) throws PatternSyntaxException {
        switch (rule.type) {
            case "files":
                runFiles(rule.path, rule.excluded_expressions);
                break;
            case "folders":
                runFolders(rule.path, rule.excluded_expressions);
                break;
            case "files|folders":
                runFiles(rule.path, rule.excluded_expressions);
                runFolders(rule.path, rule.excluded_expressions);
                break;
            default:
                break;
        }
    }

    private void runFiles(String path, ArrayList<String> excluded) throws PatternSyntaxException {
        if (new File(path).isDirectory()) {
            File[] files = new File(path).listFiles();
            for (File f : files) {
                if (f.isFile()) {
                    boolean cont = false;
                    for (String ex : excluded) {
                        Pattern patron = Pattern.compile(ex);
                        Matcher encaja = patron.matcher(f.getName());
                        if (encaja.find()) {
                            cont = true;
                            break;
                        }
                    }
                    if (cont) {
                        continue;
                    }
                    bytes_deleted += f.length();
                    number_of_files++;
                    f.delete();
                }
            }
        }
    }

    private void deleteFolder(File folder, ArrayList<String> excluded) throws PatternSyntaxException {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    boolean cont = false;
                    for (String ex : excluded) {
                        Pattern patron = Pattern.compile(ex);
                        Matcher encaja = patron.matcher(f.getName());
                        if (encaja.find()) {
                            cont = true;
                            break;
                        }

                    }
                    if (cont) {
                        continue;
                    }
                    deleteFolder(f, excluded);
                } else {
                    boolean cont = false;
                    for (String ex : excluded) {
                        Pattern patron = Pattern.compile(ex);
                        Matcher encaja = patron.matcher(f.getName());
                        if (encaja.find()) {
                            cont = true;
                            break;
                        }

                    }
                    if (cont) {
                        continue;
                    }
                    bytes_deleted += f.length();
                    number_of_files++;
                    f.delete();
                }
            }
        }
        number_of_folders++;
        folder.delete();
    }

    private void runFolders(String path, ArrayList<String> excluded) throws PatternSyntaxException {
        if (new File(path).isDirectory()) {
            File[] files = new File(path).listFiles();
            for (File f : files) {
                if (f.isFile()) {
                } else {
                    deleteFolder(f, excluded);
                }
            }
        }
    }

    /**
     * Run configure
     */
    public void config() {
        if (!new File(PATH_CONFIG_FILE).exists()) {
            try {
                createDefaultCleaner();
            } catch (ParserConfigurationException | TransformerException ex) {
                Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            Desktop.getDesktop().open(new File(PATH_CONFIG_FILE));
        } catch (IOException ex) {
            try {
                Runtime.getRuntime().exec("notepad "+PATH_CONFIG_FILE);
            } catch (IOException ex2) {
                JOptionPane.showMessageDialog(PARENT, "No se puede abrir el fichero\n" + ex2.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
         }
    }

    private static String SHORTCUT_EXPRESSION = "[\\s|\\S]*[\\.][l][n][k]";

    /**
     * Create a new configuration file.
     */
    private void createDefaultCleaner() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element root = doc.createElement("rules");
        Element excluded_lnk = doc.createElement("excluded");
        excluded_lnk.appendChild(doc.createTextNode(SHORTCUT_EXPRESSION));
        Element rule_1 = doc.createElement("rule");
        rule_1.setAttribute("type", "files|folders");
        Element path_1 = doc.createElement("path");
        path_1.appendChild(doc.createTextNode("C:\\Users\\alumno\\Desktop"));
        rule_1.appendChild(path_1);
        rule_1.appendChild(excluded_lnk.cloneNode(true));
        root.appendChild(rule_1);
        Element rule_2 = doc.createElement("rule");
        rule_2.setAttribute("type", "files|folders");
        Element path_2 = doc.createElement("path");
        path_2.appendChild(doc.createTextNode("C:\\Users\\alumno\\Downloads"));
        rule_2.appendChild(path_2);
        rule_2.appendChild(excluded_lnk.cloneNode(true));
        root.appendChild(rule_2);
        Element rule_3 = doc.createElement("rule");
        rule_3.setAttribute("type", "files");
        Element path_3 = doc.createElement("path");
        path_3.appendChild(doc.createTextNode("C:\\Users\\alumno\\Documents"));
        rule_3.appendChild(path_3);
        rule_3.appendChild(excluded_lnk.cloneNode(true));
        root.appendChild(rule_3);
        doc.appendChild(root);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(PATH_CONFIG_FILE));
        transformer.transform(source, result);
    }

    private class Rule {

        private final String type, path;
        private final ArrayList<String> excluded_expressions;

        public Rule(String type, String path, ArrayList<String> excluded) {
            this.type = type;
            this.path = path;
            this.excluded_expressions = excluded;
        }

    }
}
