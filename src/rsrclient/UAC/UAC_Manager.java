/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient.UAC;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ad_ri
 */
public class UAC_Manager {

    /**
     * check if it's running as admin
     *
     * @return true for admin false for non-admin
     */
    public boolean checkUAC() throws IOException, InterruptedException {
        Process proceso = Runtime.getRuntime().exec("cmd.exe /c net.exe session 1>NUL 2>NUL && (echo 0)|| (Echo 1)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
        proceso.waitFor();
        String linea = reader.readLine();
        return linea.equals("0");
    }

    private static final String FILE_TMP = "temp_UAC.bat";

    public void execute_as_admin() throws FileNotFoundException, IOException {
        URL o = new resources.files.FileManager().getUAC();
        File d = new File(FILE_TMP);
        String l;
        BufferedReader br = new BufferedReader(new InputStreamReader(o.openStream()));
        BufferedWriter bw = new BufferedWriter(new FileWriter(d));
        while ((l = br.readLine()) != null) {
            bw.write(l.replace("${jar}", "\"" + new File("RSRClient.jar").getAbsolutePath() + "\"").replace("${remove}", "\"" + new File(FILE_TMP).getAbsolutePath() + "\""));
            bw.write("\r\n");
        }
        bw.close();
        br.close();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Runtime.getRuntime().exec(new File(FILE_TMP).getAbsolutePath());
                } catch (IOException ex) {
                    Logger.getLogger(UAC_Manager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        new Thread(r).start();
        System.exit(0);
    }
}
