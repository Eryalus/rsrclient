/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsrclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.threads.SingleThread;

/**
 *
 * @author eryalus
 */
public class Colector extends SingleThread {

    protected final String COMANDO;
    protected final Semaphore SEM;
    private String salida = "";
    protected boolean error = false;

    public boolean hasError() {
        return error;
    }

    public Colector(String comando, Semaphore semaforo) {
        this.COMANDO = comando;
        this.SEM = semaforo;
        super.NOMBRE_HILO = "Hilo colector (" + comando + ")";
    }

    public String getSalida() {
        return salida;
    }

    public String getComando() {
        return COMANDO;
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            Process proceso = Runtime.getRuntime().exec(COMANDO);
            BufferedReader reader = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
            String linea;
            while ((linea = reader.readLine()) != null) {
                salida += linea + "\n";
            }
        } catch (IOException ex) {
            error = true;
        } finally {
            SEM.release();
        }
    }
}
