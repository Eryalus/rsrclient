
import resources.images.Images;
import rsrclient.PanelEnvio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author eryalus
 */
public class Lanzador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        looknfeel();
        PanelEnvio panel = new PanelEnvio();
        panel.setTitle("RSR Client (v" + rsrclient.RSRClient.getVers() + ")");
        panel.setIconImage(new Images().getLogoAsImage());
        panel.setLocationByPlatform(true);
        panel.setVisible(true);
        panel.autoCheck();

    }

    private static void looknfeel() {
        try {
            boolean windows = false;
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    windows = true;
                    break;
                }
            }
            if (!windows) {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Metal".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
        }
    }
}
