/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.cipher;

/**
 *
 * @author ad_ri
 */
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Encryptor {

    private IvParameterSpec IV;
    private SecretKeySpec KEY;
    private final byte PADDING = 100;

    public Encryptor(String key) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        setup(key, "0123456789101213");
    }

    public Encryptor(String key, String iv) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        setup(key, iv);
    }

    private void setup(String key, String iv) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] fkey = key.getBytes("UTF-8");
        if (fkey.length >= 16) {
            byte[] t = fkey;
            fkey = new byte[16];
            for (int i = 0; i < 16; i++) {
                fkey[i] = t[i];
            }
        } else {
            while (fkey.length % 16 != 0) {
                byte[] t = fkey;
                fkey = new byte[t.length + 1];
                for (int i = 0; i < t.length; i++) {
                    fkey[i] = t[i];
                }
                fkey[fkey.length - 1] = PADDING;
            }
        }
        byte[] fiv = iv.getBytes("UTF-8");
        if (fiv.length >= 16) {
            byte[] t = fiv;
            fiv = new byte[16];
            for (int i = 0; i < 16; i++) {
                fiv[i] = t[i];
            }
        } else {
            while (fiv.length % 16 != 0) {
                byte[] t = fiv;
                fiv = new byte[t.length + 1];
                for (int i = 0; i < t.length; i++) {
                    fiv[i] = t[i];
                }
                fiv[fiv.length - 1] = PADDING;
            }
        }
        IV = new IvParameterSpec(fiv);
        KEY = new SecretKeySpec(fkey, "AES");
    }

    public String encrypt(String value) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, KEY, IV);
        byte[] encrypted = cipher.doFinal(value.getBytes());
        return Base64.encodeBase64String(encrypted);

    }

    public byte[] encrypt(byte[] value) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, KEY, IV);
        byte[] encrypted = cipher.doFinal(value);
        return encrypted;
    }

    public byte[] decrypt(byte[] encrypted) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, KEY, IV);
        byte[] original = cipher.doFinal(encrypted);
        return original;
    }

    public String decrypt(String encrypted) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, KEY, IV);
        byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
        return new String(original);

    }

}
